import React, { Fragment } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { compose, lifecycle } from 'recompose'
import { withRouter } from 'react-router-dom'
import { get } from 'lodash'
import { connect } from 'react-redux'
import Icon from '../../components/Icon'
import Button from '../../components/Button'
import AlertModal from './components/AlertModal'
import EditModal from './components/EditModal'
import * as ActionCreators from '../../state/action-creators'
import * as Selectors from '../../state/selectors'
import Container from './components/Container'


const IconButton = styled.button`
  cursor: pointer;
  border-style: none;
  background: none;
`

const Td = styled.td`
  max-width: 200px;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  text-align: center;
`

const renderRow = (key, user, openEditModal, openConfirmModal) => (
  <tr key={key}>
    <td align="center">{user.name}</td>
    <Td align="center">{user.rg}</Td>
    <td align="center">{user.sex}</td>
    <td align="center">
      <IconButton onClick={() => openConfirmModal(user.id)}>
        <Icon className="fas fa-trash-alt" />
      </IconButton>
    </td>
    <td align="center">
      <IconButton onClick={() => openEditModal(user.id)}>
        <Icon className="fas fa-edit" />
      </IconButton>
    </td>
  </tr>
)

const renderPreLoading = () => (
  <Container>
    <div>
      <img height="500px" src="https://www.createwebsite.net/wp-content/uploads/2015/09/Display-Loading.gif" alt="Logo Geru" />
    </div>
  </Container>
)

const Details = (props) => {
  const { loading, activeUser } = props

  return (loading && !activeUser) ? renderPreLoading() : (
    <Fragment>
      <Container>
        <h1>Registros de usuários</h1>
        <Button
          color="primary"
          onClick={() => props.history.push('/cadastro')}
        >Cadastro
        </Button>
      </Container>
      <Container>
        <table>
          <thead>
            <tr>
              <th>Nome</th>
              <th>RG</th>
              <th>Sexo</th>
              <th />
              <th />
            </tr>
          </thead>
          <tbody>
            {props.users && props.users.users.map((user, key) => (
            renderRow(key, user, props.openEditModal, props.openConfirmModal)
          ))}
          </tbody>
        </table>
      </Container>
      <EditModal
        close={props.closeEditModal}
        isOpen={props.editModalIsOpen}
      />
      <AlertModal
        userId={get(props, 'activeUser.id')}
        userName={get(props, 'activeUser.name')}
        deleteUser={props.deleteUser}
        close={props.closeConfirmModal}
        isOpen={props.confirmModalIsOpen}
        isLoading={props.loading}
      />
    </Fragment>
  )
}


Details.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  activeUser: PropTypes.shape({
    id: PropTypes.string,
  }),
  users: PropTypes.shape({
    dateOfIssue: PropTypes.string,
    id: PropTypes.string,
    name: PropTypes.string,
    rg: PropTypes.string,
    sex: PropTypes.string,
    shippingAgency: PropTypes.string,
    users: PropTypes.array,
  }).isRequired,
  loading: PropTypes.bool,
  deleteUser: PropTypes.func.isRequired,
  closeConfirmModal: PropTypes.func.isRequired,
  confirmModalIsOpen: PropTypes.bool,
  openEditModal: PropTypes.func.isRequired,
  openConfirmModal: PropTypes.func.isRequired,
  closeEditModal: PropTypes.func.isRequired,
  editModalIsOpen: PropTypes.bool,
}

Details.defaultProps = {
  loading: true,
  confirmModalIsOpen: false,
  editModalIsOpen: false,
  activeUser: null,
}

const mapProps = props => ({
  users: Selectors.getUsers(props),
  activeUser: Selectors.getActiveUser(props),
  loading: Selectors.getLoading(props),
  editModalIsOpen: Selectors.getEditModalStatus(props),
  confirmModalIsOpen: Selectors.getConfirmModalStatus(props),
})

const mapActions = {
  updateUsers: ActionCreators.updateUsers,
  updateUser: ActionCreators.updateUser,
  deleteUser: ActionCreators.deleteUser,
  getUsers: ActionCreators.getUsers,
  openEditModal: ActionCreators.openEditModal,
  closeEditModal: ActionCreators.closeEditModal,
  openConfirmModal: ActionCreators.openConfirmModal,
  closeConfirmModal: ActionCreators.closeConfirmModal,
}

export default compose(
  connect(mapProps, mapActions),
  lifecycle({
    componentWillMount() {
      this.props.getUsers()
    },
  }),
  withRouter,
)(Details)
