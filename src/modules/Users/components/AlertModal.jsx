import React, { Fragment } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import Modal from 'react-modal'
import Icon from '../../../components/Icon'
import Button from '../../../components/Button'
import logo from '../../../assets/logo-geru.png'

const customStyles = {
  content: {
    fontSize: '1.5em',
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
  },
}

const Container = styled.div`
  display: flex;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 3em;
`

const ContainerConfirmButton = styled.div`
  display: flex;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 1em;
`

const ConfirmButton = styled(Button)`
  :disabled: {
    opacity: 0.5;
  }
`

const Header = styled.div`
    display: flex;
    width: calc(100% + 40px);
    display: flex;
    justify-content: space-between;
    align-items: center;
    font-size: 1.5em;
    color: white;
    background-color: ${props => props.theme.palette.default.background};
    margin: -20px;
    padding: 5px;
  `

const IconButton = styled.button`
      cursor: pointer;
      border-style: none;
      background: none;
      color: white;
    `

const Img = styled.img`
    width: 100px;
    height: auto;
  `

Modal.setAppElement('[data-js="app"]')
const Details = props => (
  <Fragment>
    <Modal
      isOpen={props.isOpen}
      style={customStyles}
      contentLabel="Example Modal"
      onRequestClose={props.close}
    >
      <Header>
        <div><Img src={logo} alt="Logo Geru" /></div>
        <IconButton onClick={props.close}>
          <Icon className="fas fa-window-close" />
        </IconButton>
      </Header>
      <Container>
        <div>Deseja realmente excluir o usuário {props.userName}?</div>
      </Container>
      <ContainerConfirmButton>
        <div>
          <ConfirmButton
            color="danger"
            disabled={props.isLoading}
            onClick={() => props.deleteUser(props.userId)}
          >{props.isLoading ? 'excluindo...' : 'Ok'}
          </ConfirmButton>
        </div>
      </ContainerConfirmButton>
    </Modal>
  </Fragment>
)

Details.propTypes = {
  isLoading: PropTypes.bool,
  userId: PropTypes.string,
  deleteUser: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired,
  userName: PropTypes.string,
  isOpen: PropTypes.bool,
}

Details.defaultProps = {
  isLoading: true,
  isOpen: false,
  userId: undefined,
  userName: undefined,
}


export default Details
