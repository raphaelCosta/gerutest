import React, { Fragment } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import Modal from 'react-modal'
import Icon from '../../../components/Icon'
import logo from '../../../assets/logo-geru.png'

const customStyles = {
  content: {
    width: '60%',
    fontSize: '1.5em',
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
  },
}

const Container = styled.div`
    display: flex;
    width: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    margin-top: 1em;
  `

const Header = styled.div`
    display: flex;
    width: calc(100% + 40px);
    display: flex;
    justify-content: space-between;
    align-items: center;
    font-size: 1.5em;
    color: white;
    background-color: ${props => props.theme.palette.default.background};
    margin: -20px;
    padding: 5px;
  `

const IconButton = styled.button`
    cursor: pointer;
    border-style: none;
    background: none;
    color: white;
  `
const Title = styled.h1`
    color: ${props => props.theme.palette.dark.default};
  `
const Img = styled.img`
    width: 100px;
    height: auto;
  `

Modal.setAppElement('[data-js="app"]')
const Details = props => (
  <Fragment>
    <Modal
      isOpen={props.isOpen}
      style={customStyles}
      contentLabel="Example Modal"
      onRequestClose={props.close}
    >
      <Header>
        <div><Img src={logo} alt="Logo Geru" /></div>
        <IconButton onClick={props.close}>
          <Icon className="fas fa-window-close" />
        </IconButton>
      </Header>
      <Container>
        <div>
          <Title>Olá!</Title>
            Ao contrário do modal de exclusão, não foi implementada a tela de edição
             do usuário pelo fato de já ter sido
            construida uma tela de formulário integrada com o Redux-Form com as validações.<br />
            Espero que seja o suficiente. <Icon className="far fa-smile" />
        </div>
      </Container>

    </Modal>
  </Fragment>
)
Details.propTypes = {
  close: PropTypes.func.isRequired,
  isOpen: PropTypes.bool,
}

Details.defaultProps = {
  isOpen: false,
}


export default Details
