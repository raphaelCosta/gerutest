import styled from 'styled-components'

const Container = styled.div`
  display: flex;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 1.5em;
  margin-top: 2em;

  table {
    width:80%;
    max-width: 800px;

  thead, tbody, tr, td, th {
    display: block;
    padding: .2em;
  }

  thead {
    background-color: #999999;
    color: #ffffff;
  }

  tr:after {
    content: ' ';
    display: block;
    visibility: hidden;
    clear: both;
  }

  thead th {
    height: 30px;
  }

  tbody {
    height: 200px;
    overflow-y: auto;
    background-color: #ecefee;
  }

  tbody td, thead th {
    width: 20%;
    float: left;
  }
}
`


export default Container
