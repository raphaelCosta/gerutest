import styled from 'styled-components'

const StylizedLink = styled.div`
  display: flex;
  width: 100%;
  background-color: #353435;
  justify-content: center;
  padding-bottom: 15px;
`

export default StylizedLink
