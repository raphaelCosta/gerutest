import PropTypes from 'prop-types'
import React from 'react'
import styled from 'styled-components'


const Container = styled.div`
  margin-left: 20px;
`
const Title = styled.p`
  font-size: 1em;
  color: ${props => props.theme.palette.dark.default};
  height: 0.3em;
  font-weight: bold;
`
const Value = styled.span`
  font-size: 2em;
  color: white;
  font-weight: bold;
`
const pageHeader = ({ title, value }) => (
  <Container>
    <Title>
      {title}
    </Title>
    <Value>
      {value}
    </Value>
  </Container>
)

pageHeader.propTypes = {
  title: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
}

export default pageHeader
