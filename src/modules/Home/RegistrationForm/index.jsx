import PropTypes from 'prop-types'
import React from 'react'
import { Field, reduxForm } from 'redux-form'
import styled from 'styled-components'
import { createTextMask } from 'redux-form-input-masks'
import { isNaN } from 'lodash'
import Input from '../../../components/Input/index'
import Select from '../../../components/Select'
import Button from '../../../components/Button'
import Icon from '../../../components/Icon'
import { shippingAgencyList, sexList } from '../constants'

const required = value => (value ? undefined : 'Campo obrigatório')
const number = value =>
  (value && isNaN(Number(value)) ? 'Apenas números' : undefined)

const Container = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  padding-top: 30px;
}
`
const DateMask = createTextMask({
  pattern: '99/99/9999',
})

const newUser = (props) => {
  const {
    handleSubmit, isLoading, history,
  } = props

  return (
    <form onSubmit={handleSubmit}>
      <Container>
        <div><h1>Dados Pessoais</h1></div>
      </Container>
      <Container>
        <Field
          name="name"
          type="text"
          component={Input}
          label="Nome"
          validate={[required]}
        />
        <Field
          name="rg"
          type="text"
          component={Input}
          label="RG"
          validate={[required, number]}
        />
        <Field
          name="dateOfIssue"
          type="text"
          component={Input}
          label="Data de emissão"
          {...DateMask}
          validate={[required]}
        />
      </Container>
      <Container>
        <Field
          name="shippingAgency"
          type="text"
          component={Select}
          label="Data de emissão"
          options={shippingAgencyList}
          validate={[required]}
        />
        <Field
          name="sex"
          type="text"
          component={Select}
          label="Sexo"
          options={sexList}
          validate={[required]}
        />
      </Container>
      <Container>
        <Button type="submit" color="primary" disabled={isLoading} >
          {props.isLoading ? 'Carregando' : 'Salvar'}
          <Icon className="fas fa-caret-right" />
        </Button>
        <Button
          type="button"
          color="default"
          onClick={() => history.push('/usuarios')}
        >Ir para a tela de listagem
        </Button>
      </Container>
    </form>
  )
}

newUser.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  isLoading: PropTypes.bool,
  pristine: PropTypes.bool.isRequired,
  handleSubmit: PropTypes.func.isRequired,
}

newUser.defaultProps = {
  isLoading: false,
}

export default reduxForm({
  form: 'users',
})(newUser)
