import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { compose } from 'recompose'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import SubHeader from './components/SubHeader'
import DisplayField from './components/DisplayField'
import Tabs from '../../components/Tabs'
import Tab from '../../components/Tabs/Tab'
import RegistrationForm from './RegistrationForm'
import * as ActionCreators from '../../state/action-creators'
import * as Selectors from '../../state/selectors'

const Details = ({ users, updateUsers, history }) => (
  <Fragment>
    <SubHeader>
      <DisplayField title="Me chamo:" value="Paul Irish" />
      <DisplayField title="Preciso de:" value="R$ 2.000" />
      <DisplayField title="Quero pagar em:" value="12 vezes" />
      <DisplayField title="Para:" value="Comprar uma bike" />
    </SubHeader>
    <Tabs>
      <Tab number="1" name="Simule" status="visited" />
      <Tab number="2" name="Preencha o cadastro" status="active" />
      <Tab number="3" name="Revise o seu pedido" status="disabled" />
      <Tab number="4" name="Finalize o pedido" status="disabled" />
    </Tabs>
    <RegistrationForm
      isLoading={users.loading}
      ooo={() => { }}
      onSubmit={updateUsers}
      history={history}
    />
  </Fragment>
)

Details.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  users: PropTypes.shape({
    dateOfIssue: PropTypes.string,
    id: PropTypes.string,
    name: PropTypes.string,
    rg: PropTypes.string,
    sex: PropTypes.string,
    shippingAgency: PropTypes.string,
  }).isRequired,
  updateUsers: PropTypes.func.isRequired,
}

const mapProps = props => ({
  users: Selectors.getUsers(props),
  loading: Selectors.getLoading(props),
})

const mapActions = {
  updateUsers: ActionCreators.updateUsers,
  updateUser: ActionCreators.updateUser,
  deleteUser: ActionCreators.deleteUser,
  getUsers: ActionCreators.getUsers,
}

export default compose(
  connect(mapProps, mapActions),
  withRouter,
)(Details)
