import { toastr } from 'react-redux-toastr'
import {
  getUsersQuery,
  updateUsersProvider,
  updateUserProvider,
  deleteUserProvider,
} from '../Providers/Users'

import {
  SET_LOADING,
  UPDATE_USERS,
  UPDATE_USER,
  SET_USERS,
  DELETE_USER,
  SET_USER_ACTIVE,
  OPEN_EDIT_MODAL,
  CLOSE_EDIT_MODAL,
  OPEN_CONFIRM_MODAL,
  CLOSE_CONFIRM_MODAL,
} from './action-types'

export const getUsers = () => (dispath) => {
  dispath({ type: SET_LOADING, loading: true })
  getUsersQuery()
    .then(resp => dispath({ type: SET_USERS, users: resp }))
}

export const setActiveUser = id => (dispath) => {
  dispath({ type: SET_USER_ACTIVE, activeUser: id })
}

export const openEditModal = id => (dispath) => {
  dispath({ type: OPEN_EDIT_MODAL })
  dispath({ type: SET_USER_ACTIVE, activeUser: id })
}

export const closeEditModal = () => (dispath) => {
  dispath({ type: CLOSE_EDIT_MODAL })
  dispath({ type: SET_USER_ACTIVE, activeUser: null })
}

export const openConfirmModal = id => (dispath) => {
  dispath({ type: OPEN_CONFIRM_MODAL })
  dispath({ type: SET_USER_ACTIVE, activeUser: id })
}

export const closeConfirmModal = () => (dispath) => {
  dispath({ type: CLOSE_CONFIRM_MODAL })
  dispath({ type: SET_USER_ACTIVE, activeUser: null })
}

export const updateUsers = user => (dispath) => {
  dispath({ type: SET_LOADING, loading: true })
  updateUsersProvider(user)
    .then(resp => dispath({ type: UPDATE_USERS, user: resp }))
    .then(() => toastr.success('Sucesso!', 'O usuario foi cadastrado'))
}

export const updateUser = (id, user) => (dispath) => {
  dispath({ type: SET_LOADING, loading: true })
  updateUserProvider(user)
    .then(resp => dispath({ type: UPDATE_USER, user: resp }))
    .then(() => dispath({ type: CLOSE_EDIT_MODAL }))
}

export const deleteUser = id => (dispath) => {
  dispath({ type: SET_LOADING, loading: true })
  deleteUserProvider(id)
    .then(resp => dispath({ type: DELETE_USER, user: resp }))
    .then(() => toastr.success('Sucesso!', 'O usuario foi removido'))
    .then(() => dispath({ type: CLOSE_CONFIRM_MODAL }))
}
