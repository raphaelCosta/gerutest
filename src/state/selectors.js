import { filter } from 'lodash'
import { createSelector } from 'reselect'

export const getUsers = ({ users }) => users
export const getActiveUser = ({ users }) => users.activeUser
export const getEditModalStatus = ({ users }) => users.editModalIsOpen
export const getConfirmModalStatus = ({ users }) => users.confirmModalIsOpen
export const getUser = ({ id }) => createSelector(getUsers, users => (
  filter(users, { id })
))
export const getLoading = ({ users }) => users.loading
