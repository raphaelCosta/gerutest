import { find, assign, reject } from 'lodash'
import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import { reducer as toastrReducer } from 'react-redux-toastr'
import {
  SET_USER_ACTIVE,
  SET_LOADING,
  UPDATE_USERS,
  UPDATE_USER,
  SET_USERS,
  DELETE_USER,
  OPEN_EDIT_MODAL,
  CLOSE_EDIT_MODAL,
  OPEN_CONFIRM_MODAL,
  CLOSE_CONFIRM_MODAL,
} from './action-types'

function reducer(state = { users: [] }, {
  type, users, user, loading, activeUser,
}) {
  switch (type) {
    case SET_LOADING: {
      return {
        ...state,
        loading,
      }
    }
    case SET_USER_ACTIVE: {
      return {
        ...state,
        activeUser: activeUser ? find(state.users, ['id', activeUser]) : undefined,
      }
    }
    case OPEN_EDIT_MODAL: {
      return {
        ...state,
        editModalIsOpen: true,
      }
    }
    case CLOSE_EDIT_MODAL: {
      return {
        ...state,
        editModalIsOpen: false,
      }
    }
    case OPEN_CONFIRM_MODAL: {
      return {
        ...state,
        confirmModalIsOpen: true,
      }
    }
    case CLOSE_CONFIRM_MODAL: {
      return {
        ...state,
        confirmModalIsOpen: false,
      }
    }
    case UPDATE_USERS: {
      return {
        ...state,
        users: [...state.users, user],
        loading: false,
      }
    }
    case UPDATE_USER: {
      return {
        ...state,
        users: assign(state.users, [user]),
        loading: false,
      }
    }
    case DELETE_USER: {
      return {
        ...state,
        users: reject(state.users, { id: user.id }),
        loading: false,
      }
    }
    case SET_USERS: {
      return {
        ...state,
        users,
        loading: false,
      }
    }

    default: return state
  }
}

const rootReducer = combineReducers({
  users: reducer,
  form: formReducer,
  toastr: toastrReducer,
})

export default rootReducer
