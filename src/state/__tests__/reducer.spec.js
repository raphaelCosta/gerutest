import reducer from '../reducer'
import { SET_LOADING, SET_USER_ACTIVE, SET_USERS, OPEN_EDIT_MODAL, CLOSE_EDIT_MODAL } from '../action-types'

it('should handle SET_LOADING', () => {
  const updateAction = {
    type: SET_LOADING,
    loading: true,
  }
  expect(reducer({}, updateAction).users.loading).toEqual(true)
})

it('should handle SET_USER_ACTIVE', () => {
  const updateActionAddUsers = {
    type: SET_USERS,
    users: [{ id: '01' }, { id: '02' }],
  }
  const updateAction = {
    type: SET_USER_ACTIVE,
    activeUser: '01',
  }
  expect(reducer(reducer({}, updateActionAddUsers), updateAction)
    .users.activeUser).toEqual({ id: '01' })
})

it('should handle OPEN_EDIT_MODAL', () => {
  const updateAction = {
    type: OPEN_EDIT_MODAL,
  }
  expect(reducer({}, updateAction).users.editModalIsOpen).toEqual(true)
})

it('should handle CLOSE_EDIT_MODAL', () => {
  const updateAction = {
    type: CLOSE_EDIT_MODAL,
  }
  expect(reducer({}, updateAction).users.editModalIsOpen).toEqual(false)
})
