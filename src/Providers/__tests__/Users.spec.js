import axios from 'axios'
import { getUsersQuery, updateUsersProvider, updateUserProvider } from '../Users'

jest.mock('axios')

test('should fetch users', () => {
  const users = [{ name: 'Bob' }]
  const resp = { data: users }
  axios.get.mockResolvedValue(resp)
  // eslint-disable-next-line no-shadow
  return getUsersQuery().then(resp => expect(resp).toEqual(users))
})

test('should post user', () => {
  const user = { name: 'Bob' }
  const resp = { data: user }
  axios.post.mockResolvedValue(resp)
  // eslint-disable-next-line no-shadow
  return updateUsersProvider(user).then(resp => expect(resp).toEqual(user))
})

test('should post user', () => {
  const editedUser = { name: 'Bobi' }
  const resp = { data: editedUser }
  axios.put.mockResolvedValue(resp)
  // eslint-disable-next-line no-shadow
  return updateUserProvider(editedUser).then(resp => expect(resp).toEqual(editedUser))
})
