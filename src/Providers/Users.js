import axios from 'axios'

const URL = 'https://5b134f5dd50a5c0014ef127a.mockapi.io/apiGeru/users'

export const getUsersQuery = () => axios.get(URL).then(resp => resp.data)

export const updateUsersProvider = user => axios.post(URL, user).then(resp => resp.data)

export const updateUserProvider = (id, user) => axios.put(`${URL}/${id}`, user).then(resp => resp.data)

export const deleteUserProvider = id => axios.delete(`${URL}/${id}`).then(resp => resp.data)
