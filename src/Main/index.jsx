import React, { Fragment } from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import Home from '../modules/Home'
import Details from '../modules/Details'
import Privacy from '../modules/Privacy'
import Help from '../modules/Help'
import Users from '../modules/Users'
import NoMatch from '../NotFound'
import PageHeader from '../components/PageHeader'
import Toastr from '../components/Toaster'

const Routes = () => (
  <Fragment>
    <main>
      <PageHeader />
      <Route exact path="/" component={() => <Redirect to="/cadastro" />} />
      <Switch>
        <Route path="/como-funciona" component={Details} />
        <Route path="/seguranca-e-privacidade" component={Privacy} />
        <Route path="/ajuda" component={Help} />
        <Route exact path="/usuarios" component={Users} />
        <Route exact path="/cadastro" component={Home} />
        <Route component={NoMatch} />
      </Switch>
      <Toastr />
    </main>
  </Fragment>
)

export default Routes
