import PropTypes from 'prop-types'
import React from 'react'
import styled from 'styled-components'

const Input = styled.input`
border: 2px solid;
font-size: 1.5em;
font-weight: bold;
padding: 10px;
color: ${props => props.theme.palette.dark.background};
background-color: white;
border-color: ${props => ((props.touched && props.error) ? '#B40404' : props.theme.palette.dark.background)};
:focus {
  border-color: ${props => (props.theme.palette.dark.default)};
  background-color: white;
  color: ${props => props.theme.palette.dark.default};
}
`

const Container = styled.div`
  margin-left: 1.5em;
  height: 70px;
  color: ${props => ((props.touched && props.error) ? '#B40404' : props.theme.palette.dark.background)};
  font-size: 1.3em;
  font-weight: bold;
  :focus-within {
    color: ${props => props.theme.palette.dark.default};
}
`

const renderField = ({
  input,
  label,
  type,
  meta: { touched, error, warning },
}) => (
  <div>
    <Container error={error} touched={touched}>
      <span>{label}</span>
      <div>
        <Input {...input} type={type} error={error} touched={touched} />
        <div>
          {touched &&
          ((error && <span>{error}</span>) ||
            (warning && <span>{warning}</span>))}
        </div>
      </div>
    </Container>
  </div>
)
renderField.propTypes = {
  input: PropTypes.shape({
    name: PropTypes.string,
  }).isRequired,
  label: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  touched: PropTypes.bool,
  error: PropTypes.string,
  warning: PropTypes.string,
  meta: PropTypes.shape({
    touched: PropTypes.bool,
    error: PropTypes.string,
  }).isRequired,
}

renderField.defaultProps = {
  touched: false,
  error: null,
  warning: null,
}

export default renderField
