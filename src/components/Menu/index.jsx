import React, { Fragment } from 'react'
import styled from 'styled-components'
import Link from '../Link'
import Icon from '../Icon'

const Row = styled.div`
  display: flex;
`
const Column = styled.div`
  margin-left: 15px;
`
const ContentIcon = styled(Icon)`
  color: ${props => props.theme.palette.dark.default};
`

const Menu = () => (
  <Fragment>
    <Row>
      <Column>
        <Link
          href="como-funciona"
          to="/como-funciona"
          name="COMO FUNCIONA"
          color="default"
        >
          COMO FUNCIONA
        </Link>
        <ContentIcon><Icon className="far fa-circle fa-xs" /></ContentIcon>
      </Column>
      <Column>
        <Link
          to="seguranca-e-privacidade"
          href="seguranca-e-privacidade"
          name="PRIVACIDADE"
          color="default"
        >
          SEGURANÇA E PRIVACIDADE
        </Link>
        <ContentIcon><Icon className="far fa-circle fa-xs" /></ContentIcon>
      </Column>
      <Column>
        <Link
          to="ajuda"
          href="ajuda"
          name="AJUDA"
          color="default"
        >
          AJUDA
        </Link>
        <ContentIcon><Icon className="far fa-circle fa-xs" /></ContentIcon>
      </Column>
    </Row>
  </Fragment>
)

Menu.defaultProps = {
  color: 'dark',
}

export default Menu
