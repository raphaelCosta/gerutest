import React from 'react'
import { shallow } from 'enzyme'
import { ThemeProvider } from 'styled-components'
import defaultTheme from '../../../../src/theme'
import Menu from '../'

function shallowWithTheme(tree, theme = defaultTheme) {
  const context = shallow(<ThemeProvider theme={theme} />)
    .instance()
    .getChildContext()

  return shallow(tree, { context })
}

test('should render a Menu', () => {
  const wrapper = shallowWithTheme(<Menu />)
  expect(wrapper).toMatchSnapshot()
})

test('should render a Menu whith primary style', () => {
  const wrapper = shallowWithTheme(<Menu color="primary" />)
  expect(wrapper).toMatchSnapshot()
})

test('should render a Menu whith default style', () => {
  const wrapper = shallowWithTheme(<Menu color="default" />)
  expect(wrapper).toMatchSnapshot()
})

test('should render a Menu whith dark style', () => {
  const wrapper = shallowWithTheme(<Menu color="dark" />)
  expect(wrapper).toMatchSnapshot()
})
