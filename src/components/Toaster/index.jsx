import React from 'react'
import ReduxToastr from 'react-redux-toastr'


const Toaster = () => (
  <ReduxToastr
    timeOut={4000}
    newestOnTop={false}
    preventDuplicates
    position="top-right"
    transitionIn="fadeIn"
    transitionOut="fadeOut"
  />
)

export default Toaster
