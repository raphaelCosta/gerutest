import React from 'react'
import styled from 'styled-components'
import Menu from '../Menu'
import logo from '../../assets/logo-geru.png'

const Row = styled.div`
  padding: 20px;
  display: flex;
`
const ColumnRight = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-end
  align-items: center;
`
const Header = styled.div`
  background-color: ${props => props.theme.palette.default.background};
  width: 100%;
  margin: 0px;
`
const Img = styled.img`
  width: 100px;
  height: auto;
`

const pageHeader = () => (
  <Header>
    <Row>
      <div>
        <Img src={logo} alt="Logo Geru" />
      </div>
      <ColumnRight>
        <Menu color="dark" />
      </ColumnRight>
    </Row>
  </Header>
)

export default pageHeader
