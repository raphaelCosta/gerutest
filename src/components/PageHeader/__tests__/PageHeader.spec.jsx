import React from 'react'
import { shallow } from 'enzyme'
import { ThemeProvider } from 'styled-components'
import defaultTheme from '../../../../src/theme'
import PageHeader from '../'

function shallowWithTheme(tree, theme = defaultTheme) {
  const context = shallow(<ThemeProvider theme={theme} />)
    .instance()
    .getChildContext()

  return shallow(tree, { context })
}

test('should render a PageHeader', () => {
  const wrapper = shallowWithTheme(<PageHeader />)
  expect(wrapper).toMatchSnapshot()
})

test('should render a PageHeader whith primary style', () => {
  const wrapper = shallowWithTheme(<PageHeader color="primary" />)
  expect(wrapper).toMatchSnapshot()
})

test('should render a PageHeader whith default style', () => {
  const wrapper = shallowWithTheme(<PageHeader color="default" />)
  expect(wrapper).toMatchSnapshot()
})

test('should render a PageHeader whith dark style', () => {
  const wrapper = shallowWithTheme(<PageHeader color="dark" />)
  expect(wrapper).toMatchSnapshot()
})
