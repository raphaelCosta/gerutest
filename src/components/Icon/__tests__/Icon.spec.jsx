import React from 'react'
import { shallow } from 'enzyme'
import { ThemeProvider } from 'styled-components'
import defaultTheme from '../../../../src/theme'
import Icon from '../'

function shallowWithTheme(tree, theme = defaultTheme) {
  const context = shallow(<ThemeProvider theme={theme} />)
    .instance()
    .getChildContext()

  return shallow(tree, { context })
}

test('should render a Circle Icon', () => {
  const wrapper = shallowWithTheme(<Icon className="far fa-circle fa-xs" />)
  expect(wrapper).toMatchSnapshot()
})
