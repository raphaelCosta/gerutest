import styled from 'styled-components'

const Icon = styled.i`
  margin-left: 4px;
`
export default Icon
