import styled from 'styled-components'

const StylizedLink = styled.div`
  display: flex;
  width: 100%;
  background-color: #E8EADC;
  justify-content: center;
`

export default StylizedLink
