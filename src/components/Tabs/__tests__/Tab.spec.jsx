import React from 'react'
import { shallow } from 'enzyme'
import { ThemeProvider } from 'styled-components'
import defaultTheme from '../../../../src/theme'
import Tab from '../Tab'
import TabContainer from '../'

function shallowWithTheme(tree, theme = defaultTheme) {
  const context = shallow(<ThemeProvider theme={theme} />)
    .instance()
    .getChildContext()

  return shallow(tree, { context })
}

test('should render a Tab', () => {
  const wrapper = shallowWithTheme(<Tab name="test" number="1" status="active" />)
  expect(wrapper).toMatchSnapshot()
})

test('should render a Tab with disabled status', () => {
  const wrapper = shallowWithTheme(<Tab name="test" number="1" status="disabled" />)
  expect(wrapper).toMatchSnapshot()
})

test('should render a Tab with visited status', () => {
  const wrapper = shallowWithTheme(<Tab name="test" number="1" status="visited" />)
  expect(wrapper).toMatchSnapshot()
})

test('should render a TabContainer', () => {
  const wrapper = shallowWithTheme(<TabContainer />)
  expect(wrapper).toMatchSnapshot()
})
