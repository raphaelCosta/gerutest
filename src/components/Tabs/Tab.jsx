import PropTypes from 'prop-types'
import React from 'react'
import styled from 'styled-components'

const NumberingStatus = {
  active: {
    background: props => props.theme.palette.default.background,
    color: props => props.theme.palette.dark.default,
    borderColor: props => props.theme.palette.default.background,
  },
  visited: {
    background: '#B0B1A7',
    color: 'white',
    borderColor: '#B0B1A7',
  },
  disabled: {
    background: 'white',
    color: '#B0B1A7',
    borderColor: '#B0B1A7',
  },
}

const Row = styled.div`
  display: flex;
  padding: 5px;
  margin-left: 10px;
  margin-right: 10px;
`
const Numbering = styled.div`
  background-color: ${props => NumberingStatus[props.status].background};
  color: ${props => NumberingStatus[props.status].color};
  height: 2em;
  border-radius: 50%;
  width: 2em;
  border: 1px solid;
  border-color: ${props => NumberingStatus[props.status].borderColor};
  display: flex;
  justify-content: center;
  align-items: center;
  margin-right: 5px;
`
const TabLabel = styled.div`
  font-weight: bold;
  font-size: 1.1em;
  color: ${props => (props.active ? props.theme.palette.dark.background : '#B0B1A7')};
  height: 2em;
  display: flex;
  align-items: center;
`


const Tabs = ({ number, name, status }) => (
  <Row>
    <Numbering status={status}>
      <div>{number}</div>
    </Numbering>
    <TabLabel status={status}>
      <div>{name}</div>
    </TabLabel>
  </Row>
)

Tabs.propTypes = {
  name: PropTypes.string.isRequired,
  number: PropTypes.string.isRequired,
  status: PropTypes.string.isRequired,
}

export default Tabs
