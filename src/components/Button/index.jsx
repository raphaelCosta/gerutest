import styled from 'styled-components'

const Button = styled.button`
  height: 40px;
  margin: 10px;
  display: flex;
  justify-content: flex-end
  align-items: center;
  font-size: 1.5em;
  font-weight: bold;
  padding-left: 30px;
  padding-right: 30px;
  padding-top: 5px;
  padding-bottom: 5px;
  cursor: pointer;
  color: ${props => props.theme.palette[props.color].default};
  border: 2px solid;
  border-color: ${props => props.theme.palette[props.color].background};
  background-color: ${props => props.theme.palette[props.color].background};
  :hover {
    border-color: ${props => props.theme.palette[props.color].hover};
    background-color: ${props => props.theme.palette[props.color].hover};
  }
  :disabled {
    opacity: 0.5;
  }
`
export default Button
