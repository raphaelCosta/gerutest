import React from 'react'
import { shallow } from 'enzyme'
import { ThemeProvider } from 'styled-components'
import defaultTheme from '../../../../src/theme'
import Button from '../'

function shallowWithTheme(tree, theme = defaultTheme) {
  const context = shallow(<ThemeProvider theme={theme} />)
    .instance()
    .getChildContext()

  return shallow(tree, { context })
}

test('should render a Button with primary style', () => {
  const wrapper = shallowWithTheme(<Button color="primary" />)
  expect(wrapper).toMatchSnapshot()
})

test('should render a Button with default style', () => {
  const wrapper = shallowWithTheme(<Button color="default" />)
  expect(wrapper).toMatchSnapshot()
})

test('should render a Button with dark style', () => {
  const wrapper = shallowWithTheme(<Button color="dark" />)
  expect(wrapper).toMatchSnapshot()
})
