import React from 'react'
import { shallow } from 'enzyme'
import { ThemeProvider } from 'styled-components'
import defaultTheme from '../../../../src/theme'
import DefaultPage from '../'

function shallowWithTheme(tree, theme = defaultTheme) {
  const context = shallow(<ThemeProvider theme={theme} />)
    .instance()
    .getChildContext()

  return shallow(tree, { context })
}

test('should render a default page with default style', () => {
  const wrapper = shallowWithTheme(<DefaultPage />)
  expect(wrapper).toMatchSnapshot()
})

test('should render a default page with primary style', () => {
  const wrapper = shallowWithTheme(<DefaultPage color="primary" />)
  expect(wrapper).toMatchSnapshot()
})

test('should render a default page with default style', () => {
  const wrapper = shallowWithTheme(<DefaultPage color="default" />)
  expect(wrapper).toMatchSnapshot()
})
