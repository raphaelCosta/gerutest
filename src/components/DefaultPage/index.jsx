import React from 'react'
import styled from 'styled-components'

const Container = styled.div`
  display: flex;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`
const Content = styled.h1`
  margin-top: 50px;
  margin-bottom: 50px;
  max-width: 900px;
  font-size: 2em;
  font-weight: bold;
  color: ${props => props.theme.palette.dark.default};
`

const DefaultPage = () => (
  <Container>
    <div>
      <Content>Esta página não é um requisito funcional para o teste,
       foi desenvolvida apenas para demonstrar a implementação do react-router-dom
      </Content>
      <Container>
        <img
          alt="Gif jovem concordando"
          src="https://www.worldofbuzz.com/wp-content/uploads/2018/04/cant-tell-if-youre-eating-fresh-sashimi-heres-how-world-of-buzz-2.gif"
        />
      </Container>
    </div>
  </Container>
)

DefaultPage.defaultProps = {
  color: 'dark',
}

export default DefaultPage
