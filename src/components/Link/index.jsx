import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Link } from 'react-router-dom'

const StylizedLink = styled(Link)`
  :link {
    color: ${props => props.theme.palette[props.color].default};
    text-decoration: none;
    cursor: ponter;
  }

  :visited {
    color: ${props => props.theme.palette[props.color].default};
  }

  :hover {
    color: ${props => props.theme.palette[props.color].hover};
    text-decoration: none;
  }

`

StylizedLink.propTypes = {
  name: PropTypes.string.isRequired,
  href: PropTypes.string.isRequired,
}

StylizedLink.defaultProps = {
  color: 'default',
}

export default StylizedLink
