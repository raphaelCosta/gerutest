import React from 'react'
import { shallow } from 'enzyme'
import { ThemeProvider } from 'styled-components'
import defaultTheme from '../../../../src/theme'
import Link from '../'

function shallowWithTheme(tree, theme = defaultTheme) {
  const context = shallow(<ThemeProvider theme={theme} />)
    .instance()
    .getChildContext()

  return shallow(tree, { context })
}

test('should render a Link', () => {
  const wrapper = shallowWithTheme(<Link to="name" name="test" href="test" />)
  expect(wrapper).toMatchSnapshot()
})

test('should render a Link whith primary style', () => {
  const wrapper = shallowWithTheme(<Link to="name" name="test" href="test" color="primary" />)
  expect(wrapper).toMatchSnapshot()
})

test('should render a Link whith default style', () => {
  const wrapper = shallowWithTheme(<Link to="name" name="test" href="test" color="default" />)
  expect(wrapper).toMatchSnapshot()
})

test('should render a Link whith dark style', () => {
  const wrapper = shallowWithTheme(<Link to="name" name="test" href="test" color="dark" />)
  expect(wrapper).toMatchSnapshot()
})
