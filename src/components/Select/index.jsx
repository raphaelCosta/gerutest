import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const Select = styled.select`
  border-color: ${props => ((props.touched && props.error) ? '#B40404' : props.theme.palette.dark.background)};
  border: 2px solid;
  font-weight: bold;
  font-size: 1.2em;
  padding: 10px;
  color: ${props => ((props.touched && props.error) ? '#B40404' : props.theme.palette.dark.background)};
  background-color: white;
  :focus {
    border-color: ${props => props.theme.palette.dark.default};
    background-color: white;
    color: ${props => props.theme.palette.dark.default};
  }
`
const Container = styled.div`
  margin-left: 1.5em;
  color: ${props => ((props.touched && props.error) ? '#B40404' : props.theme.palette.dark.background)};
  font-size: 1.3em;
  font-weight: bold;
  :focus-within {
    color: ${props => props.theme.palette.dark.default};
}
`

const renderOption = (value, label, key) => (
  <option key={key} value={value}>{label}</option>
)

const renderField = ({
  options,
  input,
  label,
  meta: { touched, error, warning },
}) => (
  <Fragment>
    <Container error={error} touched={touched}>
      <span>{label}</span>
      <div>
        <Select {...input} error={error} touched={touched} >
          {options.map((option, key) => (
            renderOption(option.value, option.label, key)
          ))}
        </Select>
        <div>
          {touched &&
        ((error && <span>{error}</span>) ||
          (warning && <span>{warning}</span>))}
        </div>
      </div>
    </Container>
  </Fragment>
)

renderField.propTypes = {
  options: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.string,
    label: PropTypes.string,
  })).isRequired,
  input: PropTypes.shape({
    name: PropTypes.string,
  }).isRequired,
  label: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  touched: PropTypes.bool,
  error: PropTypes.string,
  warning: PropTypes.string,
  meta: PropTypes.shape({
    touched: PropTypes.bool,
    error: PropTypes.string,
  }).isRequired,
}

renderField.defaultProps = {
  touched: false,
  error: null,
  warning: null,
}

export default renderField
