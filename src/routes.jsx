import React from 'react'
import { ThemeProvider } from 'styled-components'
import { Provider } from 'react-redux'
import { BrowserRouter as Router } from 'react-router-dom'
import Main from './Main'
import Theme from './theme'
import store from './store'

const Routes = () => (
  <Provider store={store}>
    <ThemeProvider theme={Theme}>
      <Router>
        <Main />
      </Router>
    </ThemeProvider>
  </Provider>

)

export default Routes
