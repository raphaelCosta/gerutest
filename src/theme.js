import { injectGlobal } from 'styled-components'
import { normalize } from 'polished'

// eslint-disable-next-line no-unused-expressions
injectGlobal`
  ${normalize()}

  html {
    font-size: 0.7em;
    font-family: "Gotham A","Gotham B",Arial;
  }

  *, ::after, ::before {
    box-sizing: border-box;
  }

  * {
    outline: none;
  }
`

export default {
  palette: {
    default: {
      default: 'white',
      hover: '#999999',
      background: '#282728',
    },
    dark: {
      default: '#37CBD5',
      hover: '#999',
      background: '#353435',
    },
    primary: {
      default: 'white',
      hover: '#37CBD5',
      background: '#00E077',
    },
    danger: {
      default: 'white',
      hover: '#FB7577',
      background: '#B40404',
    },
    secondary: {
      default: 'white',
      hover: '#848484',
      background: '#BDBDBD',
    },
  },
}
