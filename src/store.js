/* eslint-disable no-underscore-dangle */
import { applyMiddleware, createStore, compose } from 'redux'
import reduxThunk from 'redux-thunk'
import users from './state/reducer'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const middleware = applyMiddleware(reduxThunk)
const store = composeEnhancers(middleware)(createStore)

export default store(users)
