# geruTest
## Raphael Hernando
* Perfil https://www.linkedin.com/in/raphael-hernando-2ba81032/

### Requisitos
* Requisitos funcionais: https://gist.github.com/felipeduardo/187c193d909df4cd7049f8aff186ac31

### Versão do Node
* [Node 8.9.4](https://nodejs.org/en/download/package-manager/)

### Instalação
```sh
$ git clone https://raphaelCosta@bitbucket.org/raphaelCosta/gerutest.git
$ cd teste-geru
$ npm install
```

#### Ambiente de Desenvolvimento Local
Para rodar em ambiente local, utilize o comando:
```sh
$ npm start
```
O servidor web estará acessível na porta [8080](http://localhost:8080)

#### Testes
```sh
$ npm run test

A cobertura de testes é apenas para amostragem. Foram escritos testes para parte dos componentes, dos métodos da pasta provider e de parte dos reducers.

```
#### Linters
Verifica se o projeto esta normalizado com as regras de estilo de código.

```sh
$ npm run lint
```
